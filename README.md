# datagateway-api-php
PHP client/ wrapper for Peekdata.io DataGateway API

Installation:
---
```
composer require peekdata/datagateway-api-php
```

Usage:
---
```
<?php

require_once 'vendor/autoload.php';

use Peekdata\DataGatewayApi\ApiServices;
use Peekdata\DataGatewayApi\Example\Request;

$api = new ApiServices('demo.peekdata.io', 8443);

$dataRequest = Request::getExample();
var_dump(get_class_methods($dataRequest));
echo '<pre>' . $dataRequest->getPrettyPrintJson() . '</pre>';
//var_dump($dataRequest->toArray());

$response = $api->getCompatibleNodes($dataRequest);
//var_dump($response->isError());
var_dump(get_class_methods($response));
echo '<pre>' . $response->getPrettyPrintJson() . '</pre>';
//var_dump($response->toArray());
```

Notes:
---
Full usage examples included in /vendor/peekdata/datagateway-api-php/index.php file.
Copy index.php into main project directory and execute from command line or browser.