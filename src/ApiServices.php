<?php

namespace Peekdata\DataGatewayApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Peekdata\DataGatewayApi\Filesystem\File;
use InvalidArgumentException;
use Peekdata\DataGatewayApi\Model\Request\DataRequest;
use Peekdata\DataGatewayApi\Model\Response\AbstractResponse;
use Peekdata\DataGatewayApi\Model\Response\CompatibleNodesResponse;
use Peekdata\DataGatewayApi\Model\Response\DataOptimizedResponse;
use Peekdata\DataGatewayApi\Model\Response\DataResponse;
use Peekdata\DataGatewayApi\Model\Response\ErrorResponse;
use Peekdata\DataGatewayApi\Model\Response\ValidationResponse;
use Exception;

/**
 * Class ApiServices
 * @package Peekdata\DataGatewayApi
 */
class ApiServices
{
    const URI_PREFIX = '/datagateway/rest/v1';
    
    const CONST_API_METHOD_HEALTHCHECK = self::URI_PREFIX . '/healthcheck';
    const CONST_API_METHOD_GETQUERY = self::URI_PREFIX . '/query/get';
    const CONST_API_METHOD_GETDATA = self::URI_PREFIX . '/data/get';
    const CONST_API_METHOD_GETCSV = self::URI_PREFIX . '/file/get';
    const CONST_API_METHOD_GETDATA_OPTIMIZED = self::URI_PREFIX . '/data/getoptimized';
    const CONST_API_METHOD_GET_COMPATIBLE_NODES = self::URI_PREFIX . '/datamodel/getcompatible';
    const CONST_API_METHOD_VALIDATE_REQUEST = self::URI_PREFIX . '/datamodel/validate';

    const STATUS_CODE_OK = 200;
    const STATUS_CODE_INTERNAL_SERVER_ERROR = 500;

    private $client = null;

    /**
     * ApiServices constructor.
     *
     * @param string $host
     * @param int $port
     * @param string $scheme
     */
    public function __construct(string $host, int $port, string $scheme = 'https')
    {
        $this->client = new Client(
            [
                'base_uri' => sprintf('%s://%s:%s', $scheme, $host, $port),
                'timeout'  => 30,
                'headers'  => [
                    'Content-Type' => 'application/json',
                ],
            ]
        );
    }

    /**
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function healthCheck(): bool
    {
        $request = $this->buildRequest(self::CONST_API_METHOD_HEALTHCHECK, 'GET');

        try {
            $response = $this->client->send($request);

            return $response->getStatusCode() == self::STATUS_CODE_OK;

        } catch (RequestException $e) {

            return false;
        }
    }

    /**
     * @param DataRequest $dataRequest
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSelect(DataRequest $dataRequest): string
    {
        $request = $this->buildRequest(self::CONST_API_METHOD_GETQUERY);
        try {
            $requestBody = (string) $dataRequest;
            $response = $this->client->send(
                $request,
                [
                    'body' => $requestBody,
                ]
            );

            $content = $response->getBody()->getContents();

            return $content;

        } catch (RequestException $e) {

            return $e->getResponse()->getBody()->getContents();

        } catch (InvalidArgumentException $e) {

            return $e->getMessage();
        }
    }

    /**
     * @param DataRequest $dataRequest
     *
     * @return AbstractResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getData(DataRequest $dataRequest): AbstractResponse
    {
        try {
            $responseArray = $this->getArrayResponse(self::CONST_API_METHOD_GETDATA, $dataRequest);

        } catch (RequestException $e) {

            return $this->handleException($e);

        } catch (InvalidArgumentException $e) {

            return $this->handleException($e);
        }

        return new DataResponse($responseArray);
    }

    /**
     * @param DataRequest $dataRequest
     * @param string $filename
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCSV(DataRequest $dataRequest, string $filename): bool
    {
        $request = $this->buildRequest(self::CONST_API_METHOD_GETCSV);

        try {
            $response = $this->client->send(
                $request,
                [
                    'body' => (string) $dataRequest,
                ]
            );

            $content = $response->getBody()->getContents();

            return (bool) File::putContents($content, $filename);

        } catch (RequestException $e) {
            // todo : better error handling or debug
            return false;
        }
    }

    /**
     * @param DataRequest $dataRequest
     *
     * @return AbstractResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDataOptimized(DataRequest $dataRequest): AbstractResponse {
        try {
            $responseArray = $this->getArrayResponse(self::CONST_API_METHOD_GETDATA_OPTIMIZED, $dataRequest);

        } catch (RequestException $e) {

            return $this->handleException($e);

        } catch (InvalidArgumentException $e) {

            return $this->handleException($e);
        }

        return new DataOptimizedResponse($responseArray);
    }

    /**
     * @param DataRequest $dataRequest
     *
     * @return AbstractResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCompatibleNodes(DataRequest $dataRequest): AbstractResponse {
        try {
            $responseArray = $this->getArrayResponse(self::CONST_API_METHOD_GET_COMPATIBLE_NODES, $dataRequest);

        } catch (RequestException $e) {

            return $this->handleException($e);

        } catch (InvalidArgumentException $e) {

            return $this->handleException($e);
        }

        return new CompatibleNodesResponse($responseArray);
    }

    /**
     * @param DataRequest $dataRequest
     *
     * @return AbstractResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function validate(DataRequest $dataRequest): AbstractResponse {
        try {
            $responseArray = $this->getArrayResponse(self::CONST_API_METHOD_VALIDATE_REQUEST, $dataRequest);

        } catch (RequestException $e) {

            return $this->handleException($e);

        } catch (InvalidArgumentException $e) {

            return $this->handleException($e);
        }

        return new ValidationResponse($responseArray);
    }

    /**
     * @param string $uri
     * @param DataRequest $dataRequest
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getArrayResponse(string $uri, DataRequest $dataRequest): array {
        $request = $this->buildRequest($uri);

        $response = $this->client->send(
            $request,
            [
                'body' => (string) $dataRequest,
            ]
        );

        $json = $response->getBody()->getContents();
        $arrayResponse = \GuzzleHttp\json_decode($json, true);

        return $arrayResponse;
    }

    /**
     * @param string $uri
     * @param string $method
     *
     * @return Request
     */
    private function buildRequest(string $uri, string $method = 'POST'): Request {
        return new Request($method, $uri);
    }

    /**
     * @param Exception $e
     *
     * @return ErrorResponse
     */
    private function handleException(Exception $e): ErrorResponse {
        if ($e instanceof InvalidArgumentException) {

            $errorResponse = new ErrorResponse([
                'message' => $e->getMessage(),
            ]);
            $errorResponse->setStatusCode(self::STATUS_CODE_INTERNAL_SERVER_ERROR);

            return $errorResponse;
        }

        $response = $e->getResponse();

        $content = $response->getBody()->getContents();
        $responseArray = \GuzzleHttp\json_decode($content, true);
        $errorResponse = new ErrorResponse($responseArray);

        $statusCode = $response->getStatusCode();
        $errorResponse->setStatusCode($statusCode);

        return $errorResponse;
    }
}