<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 15:43
 */

namespace Peekdata\DataGatewayApi\Model;


interface ToArray {
    /**
     * @return array
     */
    public function toArray(): array;
}
