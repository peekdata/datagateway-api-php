<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 14:20
 */

namespace Peekdata\DataGatewayApi\Model\Request\Sorting;

use InvalidArgumentException;

class SortDirection {
    const ASC = 'ASC';
    const DESC = 'DESC';

    /**
     * @var string
     */
    private $direction;

    /**
     * SortDirection constructor.
     *
     * @param string $direction
     * @throws InvalidArgumentException
     */
    public function __construct(string $direction) {
        $this->validate($direction);

        $this->direction = $direction;
    }

    /**
     * @param string $direction
     *
     * @throws InvalidArgumentException
     */
    private function validate(string $direction) {
        switch ($direction) {
            case self::ASC:
            case self::DESC:
                break;

            default:
                throw new InvalidArgumentException('Invalid sorting direction: ' . $direction);
        }
    }

    /**
     * @return string
     */
    public function __toString(): string {
        return $this->direction;
    }
}
