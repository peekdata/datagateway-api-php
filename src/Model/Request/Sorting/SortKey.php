<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 14:20
 */

namespace Peekdata\DataGatewayApi\Model\Request\Sorting;


use Peekdata\DataGatewayApi\Model\ToArray;

class SortKey implements ToArray {

    /**
     * @var string
     */
    private $key;

    /**
     * @var SortDirection|null
     */
    private $sortDirection;

    /**
     * SortKey constructor.
     *
     * @param string $key
     * @param SortDirection|null $sortDirection
     */
    public function __construct(string $key, SortDirection $sortDirection = null) {
        $this->key = $key;
        $this->sortDirection = $sortDirection;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        $sortDirection = (string) $this->sortDirection;
        if (!$sortDirection) {
            return [];
        }

        return [
            'key' => $this->key,
            'direction' => $sortDirection,
        ];
    }
}
