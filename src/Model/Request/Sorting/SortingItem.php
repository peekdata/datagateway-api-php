<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 14:25
 */

namespace Peekdata\DataGatewayApi\Model\Request\Sorting;


use Peekdata\DataGatewayApi\Model\ToArray;

class SortingItem implements ToArray {

    /**
     * @var SortKey[]
     */
    private $dimensions = [];

    /**
     * @var SortKey[]
     */
    private $metrics = [];

    /**
     * @param SortKey[] $dimensions
     */
    public function setDimensions(array $dimensions) {
        $this->dimensions = $dimensions;
    }

    /**
     * @param SortKey[] $metrics
     */
    public function setMetrics(array $metrics) {
        $this->metrics = $metrics;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        $dimensionsArray = $this->getDimensionsArray();
        $metricsArray = $this->getMetricsArray();

        $sortingItemArray = [];
        if ($dimensionsArray) {
            $sortingItemArray['dimensions'] = $dimensionsArray;
        }

        if ($metricsArray) {
            $sortingItemArray['metrics'] = $metricsArray;
        }

        return $sortingItemArray;
    }

    /**
     * @return array
     */
    private function getDimensionsArray(): array {
        if (!$this->dimensions) {
            return [];
        }

        $dimensionsArray = [];
        foreach ($this->dimensions as $dimension) {
            $dimensionArray = $dimension->toArray();
            if (!$dimensionArray) {

                continue;
            }

            $dimensionsArray[] = $dimensionArray;
        }

        return $dimensionsArray;
    }

    /**
     * @return array
     */
    private function getMetricsArray(): array {
        if (!$this->metrics) {
            return [];
        }

        $metricsArray = [];
        foreach ($this->metrics as $metric) {
            $metricsArray[] = $metric->toArray();
        }

        return $metricsArray;
    }
}
