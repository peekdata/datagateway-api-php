<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 15:09
 */

namespace Peekdata\DataGatewayApi\Model\Request\Options;


use Peekdata\DataGatewayApi\Model\ToArray;

class Rows implements ToArray {

    /**
     * @var int
     */
    private $limitRowsTo;

    /**
     * Rows constructor.
     *
     * @param int $limitRowsTo
     */
    public function __construct(int $limitRowsTo) {
        $this->limitRowsTo = $limitRowsTo;
    }

    /**
     * @param int $limitRowsTo
     */
    public function setLimitRowsTo(int $limitRowsTo) {
        $this->limitRowsTo = $limitRowsTo;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
            'limitRowsTo' => $this->limitRowsTo,
        ];
    }
}
