<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 15:10
 */

namespace Peekdata\DataGatewayApi\Model\Request\Options;

use Peekdata\DataGatewayApi\Model\ToArray;
use stdClass;

class Options implements ToArray {

    /**
     * @var Rows
     */
    private $rows;
    /**
     * @var stdClass
     */
    private $arguments;

    /**
     * Options constructor.
     *
     * @param stdClass $arguments
     * @param int $limitRowsTo
     */
    public function __construct(stdClass $arguments, int $limitRowsTo = 100) {
        $this->rows = new Rows($limitRowsTo);
        $this->arguments = $arguments;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
            'rows' => $this->rows->toArray(),
            'arguments' => $this->arguments,
        ];
    }
}
