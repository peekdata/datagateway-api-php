<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 13:50
 */

namespace Peekdata\DataGatewayApi\Model\Request\Filter;


use Peekdata\DataGatewayApi\Model\ToArray;

class SingleKey implements ToArray {

    /**
     * @var string
     */
    private $key;

    /**
     * @var FilterOperation
     */
    private $filterOperation;

    /**
     * @var string[]
     */
    private $values;

    /**
     * SingleKey constructor.
     *
     * @param string $key
     * @param string $filterOperation
     * @param string[] $values
     */
    public function __construct(string $key, string $filterOperation, array $values) {
        $this->key = $key;
        $this->filterOperation = new FilterOperation($filterOperation);
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        $filterOperation = (string) $this->filterOperation;

        return [
            'key' => $this->key,
            'filterOperation' => $filterOperation ? $filterOperation : null,
            'values' => $this->values,
        ];
    }
}
