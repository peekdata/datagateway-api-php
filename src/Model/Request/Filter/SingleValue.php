<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 14:12
 */

namespace Peekdata\DataGatewayApi\Model\Request\Filter;


use Peekdata\DataGatewayApi\Model\ToArray;

class SingleValue implements ToArray {

    /**
     * @var string[]
     */
    private $keys;

    /**
     * @var string
     */
    private $value;

    /**
     * @var FilterOperation|null
     */
    private $filterOperation;

    /**
     * SingleValue constructor.
     *
     * @param string[] $keys
     * @param string $value
     * @param FilterOperation|null $filterOperation
     */
    public function __construct(array $keys, string $value, FilterOperation $filterOperation = null) {
        $this->keys = $keys;
        $this->value = $value;
        $this->filterOperation = $filterOperation;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        $filterOperation = (string) $this->filterOperation;

        return [
            'keys' => $this->keys,
            'filterOperation' => $filterOperation ? $filterOperation : null,
            'value' => $this->value,
        ];
    }
}
