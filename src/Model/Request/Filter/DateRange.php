<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 13:47
 */

namespace Peekdata\DataGatewayApi\Model\Request\Filter;


use Peekdata\DataGatewayApi\Model\ToArray;

class DateRange implements ToArray {

    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $to;
    /**
     * @var string
     */
    private $key;

    /**
     * DateRange constructor.
     *
     * @param string $from
     * @param string $to
     * @param string $key
     */
    public function __construct(string $from, string $to, string $key = '') {
        $this->from = $from;
        $this->to = $to;
        $this->key = $key;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return [
            'key' => $this->key,
            'from' => $this->from,
            'to' => $this->to,
        ];
    }
}
