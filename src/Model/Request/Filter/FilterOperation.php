<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 13:50
 */

namespace Peekdata\DataGatewayApi\Model\Request\Filter;

use InvalidArgumentException;

class FilterOperation {

    const EQUALS = 'EQUALS';
    const NOT_EQUALS = 'NOT_EQUALS';
    const STARTS_WITH = 'STARTS_WITH';
    const NOT_STARTS_WITH = 'NOT_STARTS_WITH';
    const ALL_IS_LESS = 'ALL_IS_LESS';
    const ALL_IS_MORE = 'ALL_IS_MORE';
    const AT_LEAST_ONE_IS_LESS = 'AT_LEAST_ONE_IS_LESS';
    const AT_LEAST_ONE_IS_MORE = 'AT_LEAST_ONE_IS_MORE';

    /**
     * @var string
     */
    private $filterOperation = '';

    /**
     * FilterOperation constructor.
     *
     * @param string $filterOperation
     * @throws InvalidArgumentException
     */
    public function __construct(string $filterOperation) {
        $this->validate($filterOperation);

        $this->filterOperation = $filterOperation;
    }

    /**
     * @param string $filterOperation
     *
     * @throws InvalidArgumentException
     */
    private function validate(string $filterOperation) {
        switch ($filterOperation) {
            case self::EQUALS:
            case self::NOT_EQUALS:
            case self::STARTS_WITH:
            case self::NOT_STARTS_WITH:
            case self::ALL_IS_LESS:
            case self::ALL_IS_MORE:
            case self::AT_LEAST_ONE_IS_LESS:
            case self::AT_LEAST_ONE_IS_MORE:
                break;

            default:
                throw new InvalidArgumentException('Invalid filterOperation: ' . $filterOperation);
        }
    }

    /**
     * @return string
     */
    public function __toString(): string {
        return $this->filterOperation;
    }
}
