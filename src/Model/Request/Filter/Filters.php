<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 13:46
 */

namespace Peekdata\DataGatewayApi\Model\Request\Filter;

use Peekdata\DataGatewayApi\Model\ToArray;

class Filters implements ToArray {

    /**
     * @var DateRange[]
     */
    private $dateRanges = [];

    /**
     * @var SingleKey[]
     */
    private $singleKeys = [];

    /**
     * @var SingleValue[]
     */
    private $singleValues = [];

    /**
     * @param DateRange[] $dateRanges
     */
    public function setDateRanges(array $dateRanges) {
        $this->dateRanges = $dateRanges;
    }

    /**
     * @param SingleKey[] $singleKeys
     */
    public function setSingleKeys(array $singleKeys) {
        $this->singleKeys = $singleKeys;
    }

    /**
     * @param SingleValue[] $singleValues
     */
    public function setSingleValues(array $singleValues) {
        $this->singleValues = $singleValues;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        $array = [];

        $array['dateRanges'] = $this->getDateRangesArray();
        $array['singleKeys'] = $this->getSingleKeysArray();
        $array['singleValues'] = $this->getSingleValuesArray();

        return $array;
    }

    /**
     * @return array
     */
    private function getDateRangesArray(): array {
        $dateRangesArray = [];
        foreach ($this->dateRanges as $dateRange) {
            $dateRangesArray[] = $dateRange->toArray();
        }

        return $dateRangesArray;
    }

    /**
     * @return array
     */
    private function getSingleKeysArray(): array {
        $singleKeysArray = [];
        foreach ($this->singleKeys as $singleKey) {
            $singleKeysArray[] = $singleKey->toArray();
        }

        return $singleKeysArray;
    }

    /**
     * @return array
     */
    private function getSingleValuesArray(): array {
        $singleValuesArray = [];
        foreach ($this->singleValues as $singleValue) {
            $singleValuesArray[] = $singleValue->toArray();
        }

        return $singleValuesArray;
    }
}
