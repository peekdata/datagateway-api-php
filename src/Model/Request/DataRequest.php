<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 13:34
 */

namespace Peekdata\DataGatewayApi\Model\Request;

use Peekdata\DataGatewayApi\Model\Request\Filter\Filters;
use Peekdata\DataGatewayApi\Model\Request\Options\Options;
use Peekdata\DataGatewayApi\Model\Request\Sorting\SortingItem;
use Peekdata\DataGatewayApi\Model\ToArray;

/**
 * Class DataRequest
 *
 * @package Peekdata\DataGatewayApi\Model\Request
 */
class DataRequest implements ToArray {
    /**
     * @var string
     */
    private $scopeName = '';

    /**
     * @var string
     */
    private $dataModelName = '';

    /**
     * @var string[]
     */
    private $dimensions;

    /**
     * @var string[]
     */
    private $metrics;

    /**
     * @var Filters
     */
    private $filters;

    /**
     * @var SortingItem
     */
    private $sortings;

    /**
     * @var Options
     */
    private $options;
    /**
     * @var string
     */
    private $requestId;

    /**
     * @var string
     */
    private $securityRole = '';

    public function __construct(string $requestId, string $scopeName, string $dataModelName) {
        $this->scopeName = $scopeName;
        $this->dataModelName = $dataModelName;
        $this->requestId = $requestId;
        $this->filters = new Filters();
        $this->sortings = new SortingItem();
    }

    /**
     * @param string[] $dimensions
     */
    public function setDimensions(array $dimensions) {
        $this->dimensions = $dimensions;
    }

    /**
     * @param string[] $metrics
     */
    public function setMetrics(array $metrics) {
        $this->metrics = $metrics;
    }

    /**
     * @param Filters $filters
     */
    public function setFilters(Filters $filters) {
        $this->filters = $filters;
    }

    /**
     * @param SortingItem $sortings
     */
    public function setSortings(SortingItem $sortings) {
        $this->sortings = $sortings;
    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options) {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        $sortings = $this->sortings->toArray();

        $request = [
            'requestID' => $this->requestId,
            'consumerInfo' => [
                'ApplicationName' => 'Example PHP Client',
            ],

            'scopeName' => $this->scopeName,
            'dataModelName' => $this->dataModelName,

            'dimensions' => $this->dimensions,
            'metrics' => $this->metrics,

            'filters' => $this->filters->toArray(),
            'sortings' => $sortings ? $sortings : new \stdClass(),
            'options' => $this->options->toArray(),
            'securityRole' => $this->securityRole,
        ];

        return $request;
    }

    /**
     * @return string
     */
    public function __toString(): string {
        $request = $this->toArray();

        return \GuzzleHttp\json_encode($request);
    }

    /**
     * @return string
     */
    public function getPrettyPrintJson(): string {
        $request = $this->toArray();

        return \GuzzleHttp\json_encode($request, JSON_PRETTY_PRINT);
    }

    /**
     * @return string
     */
    public function getSecurityRole(): string {
        return $this->securityRole;
    }

    /**
     * @param string $securityRole
     */
    public function setSecurityRole(string $securityRole) {
        $this->securityRole = $securityRole;
    }
}
