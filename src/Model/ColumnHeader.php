<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 11:52
 */

namespace Peekdata\DataGatewayApi\Model;


class ColumnHeader {

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $dataType = '';

    /**
     * @var string|null
     */
    public $format;

    /**
     * @var string
     */
    public $alias = '';

    /**
     * ColumnHeader constructor.
     *
     * @param array $columnHeaderItem
     */
    public function __construct(array $columnHeaderItem) {
        $this->title = $columnHeaderItem['title'];
        $this->name = $columnHeaderItem['name'];
        $this->dataType = $columnHeaderItem['dataType'];
        $this->format = $columnHeaderItem['format'];
        $this->alias = $columnHeaderItem['alias'];
    }

    /**
     * @param array $columnHeaderItems
     *
     * @return ColumnHeader[]
     */
    public static function generateColumnHeaders(array $columnHeaderItems): array {
        $columnHeaders = [];
        foreach ($columnHeaderItems as $columnHeaderItem) {
            $columnHeader = new ColumnHeader($columnHeaderItem);
            $columnHeaders[] = $columnHeader;
        }

        return $columnHeaders;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDataType(): string {
        return $this->dataType;
    }

    /**
     * @return string|null
     */
    public function getFormat() {
        return $this->format;
    }

    /**
     * @return string
     */
    public function getAlias(): string {
        return $this->alias;
    }
}