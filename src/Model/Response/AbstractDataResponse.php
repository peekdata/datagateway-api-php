<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 12:03
 */

namespace Peekdata\DataGatewayApi\Model\Response;


use Peekdata\DataGatewayApi\Model\ColumnHeader;

abstract class AbstractDataResponse extends AbstractResponse {

    /**
     * @var string
     */
    protected $requestID = '';

    /**
     * @var ColumnHeader[]
     */
    protected $columnHeaders = [];

    /**
     * @var array
     */
    protected $rows = [];

    /**
     * @var int
     */
    protected $totalRows = 0;

    /**
     * AbstractDataResponse constructor.
     *
     * @param array $dataResponseItem
     */
    public function __construct(array $dataResponseItem) {
        parent::__construct($dataResponseItem);

        $this->requestID = $dataResponseItem['requestID'];
        $this->columnHeaders = ColumnHeader::generateColumnHeaders($dataResponseItem['columnHeaders']);
        $this->rows = $dataResponseItem['rows'];
        $this->totalRows = $dataResponseItem['totalRows'];
    }

    /**
     * @return string
     */
    public function getRequestID(): string {
        return $this->requestID;
    }

    /**
     * @return ColumnHeader[]
     */
    public function getColumnHeaders(): array {
        return $this->columnHeaders;
    }

    /**
     * @return array
     */
    public function getRows(): array {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function getTotalRows(): int {
        return $this->totalRows;
    }
}
