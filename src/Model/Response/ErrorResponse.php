<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 21:42
 */

namespace Peekdata\DataGatewayApi\Model\Response;


class ErrorResponse extends AbstractResponse {

    /**
     * @var int
     */
    private $statusCode = 0;

    /**
     * @var bool
     */
    protected $isError = true;

    /**
     * @return int
     */
    public function getStatusCode(): int {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode) {
        $this->statusCode = $statusCode;
    }
}
