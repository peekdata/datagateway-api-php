<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 12:03
 */

namespace Peekdata\DataGatewayApi\Model\Response;


use Peekdata\DataGatewayApi\Model\ToArray;

abstract class AbstractResponse implements ToArray {

    /**
     * @var array
     */
    protected $responseArray = [];

    /**
     * @var bool
     */
    protected $isError = false;

    /**
     * AbstractResponse constructor.
     *
     * @param array $dataResponseItem
     */
    public function __construct(array $dataResponseItem) {
        $this->responseArray = $dataResponseItem;
    }

    /**
     * @return array
     */
    public function toArray(): array {
        return $this->responseArray;
    }

    /**
     * @return string
     */
    public function getPrettyPrintJson(): string {
        return \GuzzleHttp\json_encode($this->responseArray, JSON_PRETTY_PRINT);
    }

    /**
     * @return bool
     */
    public function isError(): bool {
        return $this->isError;
    }
}

