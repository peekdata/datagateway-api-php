<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 12:38
 */

namespace Peekdata\DataGatewayApi\Model\Response;


class ValidationResponse extends AbstractResponse {

    /**
     * @var string
     */
    private $requestID = '';

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var int
     */
    private $errorCode = 0;

    /**
     * @var bool
     */
    private $isValid = false;

    /**
     * ValidationResponse constructor.
     *
     * @param array $validationItem
     */
    public function __construct(array $validationItem) {
        parent::__construct($validationItem);

        $this->requestID = $validationItem['requestID'];
        $this->message = $validationItem['message'];
        $this->errorCode = $validationItem['errorCode'];
        $this->isValid = $validationItem['isValid'];
    }

    /**
     * @return string
     */
    public function getRequestID(): string {
        return $this->requestID;
    }

    /**
     * @return null|string
     */
    public function getMessage(): string {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getErrorCode(): int {
        return $this->errorCode;
    }

    /**
     * @return bool
     */
    public function isValid(): bool {
        return $this->isValid;
    }
}
