<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 12:25
 */

namespace Peekdata\DataGatewayApi\Model\Response;


use Peekdata\DataGatewayApi\Model\Node;

class CompatibleNodesResponse extends AbstractResponse {

    /**
     * @var string
     */
    private $requestID = '';

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var int
     */
    private $errorCode = 0;

    /**
     * @var Node[]
     */
    private $dimensions = [];

    /**
     * @var Node[]
     */
    private $metrics = [];

    /**
     * CompatibleNodesResponse constructor.
     *
     * @param array $compatibleNodesResponseItem
     */
    public function __construct(array $compatibleNodesResponseItem) {
        parent::__construct($compatibleNodesResponseItem);

        $this->requestID = $compatibleNodesResponseItem['requestID'];
        $this->errorCode = $compatibleNodesResponseItem['errorCode'];

        $this->dimensions = Node::generateNodes($compatibleNodesResponseItem, 'dimensions');
        $this->metrics = Node::generateNodes($compatibleNodesResponseItem, 'metrics');
    }

    /**
     * @return string
     */
    public function getRequestID(): string {
        return $this->requestID;
    }

    /**
     * @return null|string
     */
    public function getMessage(): string {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getErrorCode(): int {
        return $this->errorCode;
    }

    /**
     * @return Node[]
     */
    public function getDimensions(): array {
        return $this->dimensions;
    }

    /**
     * @return Node[]
     */
    public function getMetrics(): array {
        return $this->metrics;
    }
}
