<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 12:22
 */

namespace Peekdata\DataGatewayApi\Model;


class Node {

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var array
     */
    private $groups = [];

    /**
     * @var string
     */
    private $dataType = '';

    /**
     * Node constructor.
     *
     * @param array $nodeItem
     */
    public function __construct(array $nodeItem) {
        $this->name = $nodeItem['name'];
        $this->title = $nodeItem['title'];
        $this->description = $nodeItem['description'];
        $this->groups = $nodeItem['groups'];
        $this->dataType = $nodeItem['dataType'];
    }

    /**
     * @param array $response
     * @param string $key
     *
     * @return array
     */
    public static function generateNodes(array $response, string $key) {
        if (empty($response[$key])) {
            return [];
        }

        $nodes = [];
        foreach ($response[$key] as $nodeItem) {
            $node = new Node($nodeItem);
            $nodes[] = $node;
        }

        return $nodes;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getGroups(): array {
        return $this->groups;
    }

    /**
     * @return string
     */
    public function getDataType(): string {
        return $this->dataType;
    }
}
