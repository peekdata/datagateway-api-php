<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 09/02/2019
 * Time: 15:07
 */

namespace Peekdata\DataGatewayApi\Filesystem;

/**
 * Class File
 *
 * @package Peekdata\DataGatewayApi\Filesystem
 */
class File {

    /**
     * @param string $directory
     * @param string $filename
     *
     * @return string
     */
    public static function getContents(string $filename, string $directory = ''): string {
        $path = self::getPath($filename, $directory);

        $content = file_get_contents($path);

        return $content;
    }

    /**
     * @param string $content
     * @param string $filename
     * @param string $directory
     *
     * @return bool
     */
    public static function putContents(string $content, string $filename, string $directory = ''): bool {
        $path = self::getPath($filename, $directory);

        $result = file_put_contents($path, $content);

        return (bool) $result;
    }

    /**
     * @param string $filename
     * @param string $directory
     *
     * @return string
     */
    private static function getPath(string $filename, string $directory): string {
        if ($directory) {
            return sprintf(
                '%s%s%s%s%s',
                getcwd(),
                DIRECTORY_SEPARATOR,
                $directory,
                DIRECTORY_SEPARATOR,
                $filename
            );
        }

        return sprintf(
            '%s%s%s',
            getcwd(),
            DIRECTORY_SEPARATOR,
            $filename
        );
    }
}
