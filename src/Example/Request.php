<?php
/**
 * Created by PhpStorm.
 * User: aleksander.hitrov
 * Date: 10/02/2019
 * Time: 13:44
 */

namespace Peekdata\DataGatewayApi\Example;


use Peekdata\DataGatewayApi\Model\Request\Filter\DateRange;
use Peekdata\DataGatewayApi\Model\Request\Filter\Filters;
use Peekdata\DataGatewayApi\Model\Request\Filter\FilterOperation;
use Peekdata\DataGatewayApi\Model\Request\Filter\SingleKey;
use Peekdata\DataGatewayApi\Model\Request\Filter\SingleValue;
use Peekdata\DataGatewayApi\Model\Request\Options\Options;
use Peekdata\DataGatewayApi\Model\Request\DataRequest;
use Peekdata\DataGatewayApi\Model\Request\Sorting\SortDirection;
use Peekdata\DataGatewayApi\Model\Request\Sorting\SortingItem;
use Peekdata\DataGatewayApi\Model\Request\Sorting\SortKey;
use stdClass;

/**
 * Class Request
 *
 * @package Peekdata\DataGatewayApi\Example
 */
class Request {

    /**
     * @return DataRequest
     */
    public static function getExample(): DataRequest {
        $scopeName = 'Mortgage-Lending';
        $dataModelName = 'Servicing-PostgreSQL';

        $requestId = rand(10000, 99999);
        $dataRequest = new DataRequest($requestId, $scopeName, $dataModelName);

        self::setDimensions($dataRequest);
        self::setMetrics($dataRequest);
        self::setFilters($dataRequest);
        self::setSortings($dataRequest);

        $arguments = new stdClass();
        self::setOptions($dataRequest, $arguments);

        return $dataRequest;
    }

    /**
     * @param DataRequest $dataRequest
     */
    private static function setDimensions(DataRequest $dataRequest) {
        $dimensions = [
            'cityname',
            'currency',
            'countryname',
        ];

        $dataRequest->setDimensions($dimensions);
    }

    /**
     * @param DataRequest $dataRequest
     */
    private static function setMetrics(DataRequest $dataRequest) {
        $metrics = [
            'loanamount',
            'propertyprice',
        ];

        $dataRequest->setMetrics($metrics);
    }

    /**
     * @param DataRequest $dataRequest
     */
    private static function setFilters(DataRequest $dataRequest) {
        $dateRanges = [
            new DateRange('2015-01-01', '2017-12-31', 'closingdate'),
        ];

        $singleKeys = [
            new SingleKey('propertycityid', FilterOperation::EQUALS, [
                '30',
                '35',
                '26',
            ]),
            new SingleKey('countryname', FilterOperation::EQUALS, [
                'Poland',
            ]),
        ];

        $singleValues = [
            new SingleValue([
                'closingdate',
                'approvaldate',
            ], '2012-12-25'),
        ];

        $filters = new Filters();
        $filters->setDateRanges($dateRanges);
        $filters->setSingleKeys($singleKeys);
        $filters->setSingleValues($singleValues);

        $dataRequest->setFilters($filters);
    }

    /**
     * @param DataRequest $dataRequest
     */
    private static function setSortings(DataRequest $dataRequest) {
        $sortingItem = new SortingItem();
        $sortingItem->setDimensions([
            new SortKey('currency', new SortDirection(SortDirection::ASC)),
            new SortKey('countryname'),
            new SortKey('cityname', new SortDirection(SortDirection::DESC)),
        ]);

        //$sortingItem->setMetrics([
        //    new SortKey(),
        //]);

        $dataRequest->setSortings($sortingItem);
    }

    /**
     * @param DataRequest $dataRequest
     * @param stdClass $arguments
     */
    private static function setOptions(DataRequest $dataRequest, stdClass $arguments) {
        $dataRequest->setOptions(new Options($arguments));
    }
}
