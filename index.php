<?php

require_once 'vendor/autoload.php';

use Peekdata\DataGatewayApi\ApiServices;
use Peekdata\DataGatewayApi\Example\Request;

$dataRequest = Request::getExample();

echo "<h2>=== Request JSON ===</h2>";
echo '<pre>' . $dataRequest->getPrettyPrintJson() . '</pre>';
echo "<br><hr>";

$api = new ApiServices('demo.peekdata.io', 8443);

echo "<h2>=== healthCheck ===</h2>";
$healthCheckResponse = $api->healthCheck();
var_dump ($healthCheckResponse);
echo "<br><hr>";

echo "<h2>=== getSelect ===</h2>";
$getSelectResponse = $api->getSelect($dataRequest);
echo '<pre>' . $getSelectResponse . '</pre>';
echo "<br><hr>";

echo "<h2>=== getData ===</h2>";
$getDataResponse = $api->getData($dataRequest);
var_dump($getDataResponse->isError());
echo '<pre>' . $getDataResponse->getPrettyPrintJson() . '</pre>';
echo "<br><hr>";

echo "<h2>=== getCSV ===</h2>";
$getCSVResponse = $api->getCSV($dataRequest, 'test.csv');
var_dump ($getCSVResponse);
echo "<br><hr>";

echo "<h2>=== getDataOptimized ===</h2>";
$getDataOptimizedResponse = $api->getDataOptimized($dataRequest);
var_dump($getDataOptimizedResponse->isError());
echo '<pre>' . $getDataOptimizedResponse->getPrettyPrintJson() . '</pre>';
echo "<br><hr>";

echo "<h2>=== validate ===</h2>";
$validateResponse = $api->validate($dataRequest);
var_dump($validateResponse->isError());
echo '<pre>' . $validateResponse->getPrettyPrintJson() . '</pre>';
echo "<br><hr>";

echo "<h2>=== getCompatibleNodes ===</h2>";
$getCompatibleNodesResponse = $api->getCompatibleNodes($dataRequest);
var_dump($getCompatibleNodesResponse->isError());
echo '<pre>' . $getCompatibleNodesResponse->getPrettyPrintJson() . '</pre>';
echo "<br><hr>";